﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityTrigger : MonoBehaviour
{

    public Transform SpaceCraftTransform;

    void OnTriggerStay(Collider other)
    {
        // Pull towards planet
        
        Debug.Log("Gravity has entered the Chat");
        SpaceCraftTransform.position = Vector3.MoveTowards(SpaceCraftTransform.position, transform.position, 0.05f);
    }
}
